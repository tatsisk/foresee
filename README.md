# foresee

This repository contains the toolbox for the Bayesian Network implementation of the FORESEE project, on the Fiumarella bridge.

## Getting started

- Pull down a copy of the toolbox by cloning or downloading the repository
- Access the toolbox through the Jupyter notebook ```foresse.ipynb```

## Retraining Dependencies

The toolbox contains the following software and package dependencies, which are required for retraining the Bayesian Network using different earthquake events and model parameters:

#### SAP2000 

The [SAP2000](https://www.csiamerica.com/products/sap2000) sofware for structural analysis and design, in which the numerical model of the Fiumarella bridge is analysed.

#### comtypes

The python COM package, which allows to define, call, and implement custom and dispatch-based COM interfaces in pure Python:

```
pip install comtypes
```